var Customer = require('../../models/user');

exports.customerView = function (req, res, next) {
    Customer.find(function(err, customers) {
        res.render('contents/admin/customer-view', { title: 'Quản lý người dùng', layout: 'ad_layout', customers: customers});
    });
};

exports.customerDetail = function (req, res, next) {
    var id = req.params.id;

    Customer.findOne({_id: id}, function (err, customer) {
        if (customer) {
            res.render('contents/admin/customer-detail', {title: 'Thông tin người dùng', layout:'ad_layout', customer: customer});
        }
    });
};

exports.customerChangeStatus = function (req, res, next) {
    var id = req.body.customerID;
    var status = req.body.customerStatus;

    console.log(status);

    Customer.findOne({_id: id}, function(err, customer) {
        if (err) {
            console.log(err.message);
        }

        customer.isAuthenticated = status;

        console.log(customer.isAuthenticated);
        customer.save(function (err) {
            if(err) {
                console.log(err.message);
            }
            res.redirect('/admin/customer-detail/' + id);
        });
    });
};

exports.customerFind = function (req, res, next) {
    var _email = req.body.searchEmail;

    Customer.find({email: _email}, function (err, customers) {
        if (err) {
            console.log(err.message);
        }
        res.render('contents/admin/customer-view', { title: 'Quản lý người dùng', layout: 'ad_layout', customers: customers});
    });
};
