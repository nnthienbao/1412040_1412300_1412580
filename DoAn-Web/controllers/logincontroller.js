var passport = require('passport');
var User = require('../models/user');
var MailVeriry = require('../helpers/mail-verify');

exports.register = function (req, res, next) {
    req.checkBody('email', 'Mail không hợp lệ').notEmpty().isEmail();
    req.checkBody('password', 'Mật khẩu ít nhất 4 ký tự').notEmpty().isLength({min: 4});
    req.checkBody('fullname', 'Họ tên không hợp lệ').notEmpty();
    req.checkBody('address', 'Địa chỉ không hợp lệ').notEmpty();
    req.checkBody('repassword', 'Mật khẩu xác thực không khớp').equals(req.body.password);
    var messages = [];
    var errors = req.validationErrors();
    if (errors) {
        errors.forEach(function (error) {
            messages.push(error.msg);
        })
        return res.render('contents/register', {
            title: 'Đăng ký',
            messages: messages, hasError: messages.length > 0,
            email: req.body.email,
            password: req.body.password,
            repassword: req.body.repassword,
            fullname: req.body.fullname,
            address: req.body.address,
            phone: req.body.phone
        });
    }
    var gender = req.body.gender == 'male' ? true : false;
    User.register(new User({
            email: req.body.email,
            fullname: req.body.fullname,
            gender: gender,
            address: req.body.address,
            phone: req.body.phone,
		role: 'user'
        }), req.body.password, function (err, user) {
            if (err) {
                console.log(err.message);
                messages.push(err.message);
                return res.render('contents/register', {
                    title: 'Đăng ký',
                    messages: messages, hasError: messages.length > 0,
                    email: req.body.email,
                    password: req.body.password,
                    repassword: req.body.repassword,
                    fullname: req.body.fullname,
                    address: req.body.address,
                    phone: req.body.phone
                });
            }
            //send email verification
            MailVeriry.send(req, res, next, user);
        });
}

exports.verify = function (req, res, next) {
    User.verifyEmail(req.query.authToken, function (err, existingAuthToken) {
        if (err) console.log('err:', err);

        res.render('contents/email-verification', {title: 'Xác thực email thành công', success: true});
    });
}
