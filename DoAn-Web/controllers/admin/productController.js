var mongoose = require('mongoose');
var Product = require('../../models/product');
var Maker = require('../../models/maker');
var specification = require('../../models/specification');

//  Get method
exports.productView = function (req, res, next) {
    Product.find(function (err, docs) {
        res.render('contents/admin/product-view', {title: 'Danh sách sản phẩm', layout: 'ad_layout', products: docs});
    });
};

exports.productInsertView = function (req, res, next) {

    Maker.find(function (err, makers) {
        if (err) {
            console.log(err.message);
        }
        res.render('contents/admin/add-new-product', {title: 'Thêm mới sản phẩm', layout: 'ad_layout', makers: makers});
    })
}


exports.productDetail = function (req, res, next) {
    var id = req.params.id;


    Product.findOne({_id: id}).populate('specification').populate('maker').exec(function (err, product) {
        if (err) {
            console.log('Error: ' + err.message);
        }
        res.render('contents/admin/product-detail', {
            title: 'Chi tiết sản phẩm',
            layout: 'ad_layout',
            product: product,
            input: 'readonly'
        });
    });
};

exports.productEdit = function (req, res, next) {
    var id = req.params.id;//req.query.id;

    var makers;

    Maker.find(function (err, docs) {
        makers = docs;
    });

    Product.findOne({_id: id}).populate('specification').populate('maker').exec(function (err, product) {
        if (err) {
            console.log('Error: ' + err.message);
        }
        console.log(product.maker.nameMaker);
        res.render('contents/admin/edit-product', {
            title: 'Chỉnh sửa sản phẩm',
            layout: 'ad_layout',
            product: product,
            makers: makers,
            nameMakerOfProduct: product.maker.nameMaker
        });
    });
};

//  Post method
exports.productInsert = function (req, res, next) {

    req.checkBody('tensp', 'Tên sản phẩm không hợp lệ').notEmpty();
    req.checkBody('giaban', 'Giá bán không hợp lệ').isInt();
    req.checkBody('soluong', 'Tên sản phẩm không hợp lệ').notEmpty();

    req.checkBody('kichthuocsp', 'Tên sản phẩm không hợp lệ').notEmpty();
    req.checkBody('kichthuocmh', 'Kích thước màn hình không hợp lệ').notEmpty();
    req.checkBody('hedieuhanh', 'Tên hệ điều hành không hợp lệ').notEmpty();
    req.checkBody('vixuly', 'CPU không hợp lệ').notEmpty();
    req.checkBody('bonhotrong', 'Bộ nhớ trong không hợp lệ').isInt();
    req.checkBody('bonhoram', 'Bộ nhớ ram không hợp lệ').isInt();
    req.checkBody('cameratruoc', 'Camera trước không hợp lệ').notEmpty();
    req.checkBody('camerasau', 'Camera sau không hợp lệ').notEmpty();
    req.checkBody('dungluongpin', 'Dung lượng pin không hợp lệ').notEmpty();
    req.checkBody('ketnoi', 'Chế độ kết nối không hợp lệ').notEmpty();

    var errors = req.validationErrors();
    var messages = [];
    if(errors) {
        errors.forEach(function (err) {
            messages.push(err.msg);
        });
        return res.render('contents/admin/add-new-product', {
            layout: "ad_layout",
            title: "Thêm mới sản phẩm",
            messages: messages
        });
    }

    // Luu anh
    var imgMain = req.files.imgmain[0];
    var imgExtra1 = req.files.imgExtra1[0];
    var imgExtra2 = req.files.imgExtra2[0];
    var imgExtra3 = req.files.imgExtra3[0];


    var specification_instance = new specification({
        operatingSystem: req.body.hedieuhanh,
        monitor: req.body.kichthuocmh,
        cpu: req.body.vixuly,
        ram: req.body.bonhoram,
        internalMemory: req.body.bonhotrong,
        backCamera: req.body.camerasau,
        frontCamera: req.body.cameratruoc,
        pin: req.body.dungluongpin,
        connect: req.body.ketnoi,
        size: req.body.kichthuocsp
    });
    specification_instance.save(function(err) {
        if(err) {
            messages.push('Thêm sản phẩm lỗi');
            return res.render('contents/admin/add-new-product', {
                layout: "ad_layout",
                title: "Thêm mới sản phẩm",
                messages: messages
            });
        }
        Maker.findById(req.body.hangsanxuat, function (err, maker){
            if(err) {
                messages.push('Thêm sản phẩm lỗi');
                return res.render('contents/admin/add-new-product', {
                    layout: "ad_layout",
                    title: "Thêm mới sản phẩm",
                    messages: messages
                });
            }
            var name = req.body.tensp;
            var product_instance = new Product({
                _id: name.split(' ').join('-'),
                productName: req.body.tensp,
                price: parseInt(req.body.giaban),
                quantity: req.body.soluong,
                imgMain: '/images/' + imgMain.filename,
                imgExtra1: '/images/' + imgExtra1.filename,
                imgExtra2: '/images/' + imgExtra2.filename,
                imgExtra3: '/images/' + imgExtra3.filename,
                specification: specification_instance,
                maker: maker,
                countSell: 0,
                dateAdd: new Date()
            });
            product_instance.save(function(error) {
                if (error) {
                    messages.push('Thêm sản phẩm lỗi');
                    return res.render('contents/admin/add-new-product', {
                        layout: "ad_layout",
                        title: "Thêm mới sản phẩm",
                        messages: messages
                    });
                }
                res.render('contents/admin/add-new-product', {
                    layout: "ad_layout",
                    title: "Thêm mới sản phẩm",
                    success: true
                });
            });
        });
    });
};

exports.productUpdate = function (req, res, next) {
    console.log()
};

exports.productDelete = function (req, res, next) {
    var id = req.body.id;
    Product.findOneAndRemove({_id: id}, function (err) {
        if(err) {
            console.log(err.message);
        }
        return res.redirect('/admin/product-view');
    });
    console.log()
};



















