var Product = require('../models/product');
var Maker = require('../models/maker');
var Cart = require('../models/cart');

exports.ProductView = function (req, res, next) {

    var itemOnePage = 9;

    var options = {};
    var keyword = req.query.keyword;
    if (keyword != null && keyword != '') {
        options.productName = {"$regex": keyword, "$options": "i"};
    }
    var idMaker = req.params.idMaker;
    if (idMaker != 'all') {
        options.maker = idMaker;
    }
    var idPage = req.query.page;
    if (idPage == null) {
        idPage = 1;
    }
    var filterPrice = req.query.price;
    switch (filterPrice) {
        case 'less1':
            options.price = {$gt: 0, $lt: 999999};
            break;
        case '1-3':
            options.price = {$gt: 1000000, $lt: 3000000};
            break;
        case '3-7':
            options.price = {$gt: 3000000, $lt: 7000000};
            break;
        case '7-10':
            options.price = {$gt: 7000000, $lt: 10000000};
            break;
        case 'more10':
            options.price = {$gt: 10000000};
            break;
    }

    Product.find(options, function (err, productsTotal) {
        var totalProduct = productsTotal.length;
        var totalPage = parseInt((totalProduct - 1) / itemOnePage) + 1;
        if (idPage > totalPage) {
            idPage = 1;
        }
        Product.find(options)
            .skip((idPage - 1) * itemOnePage)
            .limit(itemOnePage)
            .exec(function (err, products) {
                res.render('contents/productsview', {
                    title: 'Sản phẩm', products: products,
                    totalPage: totalPage, idMaker: req.params.idMaker, currentPage: idPage,
                    filterPrice: filterPrice,
                    keyword: keyword,
                    totalProduct: totalProduct,
                });
            });
    });
};

exports.ProductSearch = function (req, res, next) {
    var keyword = req.body.keyword;
    var idMaker = req.body.maker;
    var filterPrice = req.body.price;

    var fullUrl = req.protocol + '://' + req.get('host') + '/productsview/' + idMaker + '?';
    if (keyword != '') {
        fullUrl += 'keyword=' + keyword;
    }
    if (filterPrice != 'all') {
        fullUrl += '&price=' + filterPrice;
    }
    fullUrl += '&page=1';
    return res.redirect(fullUrl);
};

exports.ProductDetail = function (req, res, next) {
    var id = req.query.id;
    Product.findOne({_id: id}).populate('specification').populate('maker').exec(function (err, product) {
        if (err) {
            console.log('Error: ' + err.message);
        }
        res.render('contents/product-detail', {title: 'Thông tin sản phẩm', product: product});
    });
};

