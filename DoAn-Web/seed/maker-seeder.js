var mongoose = require('mongoose');
var Maker = require('../models/maker');

mongoose.connect('localhost:27017/mobileshop');

var iphone = new Maker({
   _id: 'iphone',
    nameMaker: 'Iphone'
});

var asus = new Maker({
    _id: 'asus',
    nameMaker: 'Asus'
});

var huawei = new Maker({
    _id: 'huawei',
    nameMaker: 'Huawei'
});

iphone.save();
asus.save();
huawei.save();


mongoose.disconnect();