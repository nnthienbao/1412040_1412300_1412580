var Specification = require('../models/specification');
var Maker = require('../models/maker');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    _id: {type: String},
    productName: {type: String, required: true},
    price: {type: Number, required: true},
    quantity: {type: Number, required: true},
    imgMain: {type: String},
    imgExtra1: {type: String},
    imgExtra2: {type: String},
    imgExtra3: {type: String},
    specification: {type: Schema.ObjectId, ref: 'Specification'},
    maker: {type: String, ref: 'Maker'},
    countSell: {type: Number},
    dateAdd: {type: Date}
});

module.exports = mongoose.model('Product', schema);