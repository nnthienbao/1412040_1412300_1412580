var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    _id: {type: String, required: true, unique: true},
    nameMaker: {type: String, required: true}
});

module.exports = mongoose.model('Maker', schema);