var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('../models/user');
var autoIncrement = require('mongoose-auto-increment');
var searchable = require('mongoose-searchable');

var schema = new Schema({
    user: {type: Number, ref: 'User'},
    cart: {type: Object, required: true},
    address: {type: String, required: true},
    phone: {type: String, required: true},
    date: {type: Date, required: true},
    status: {type: String, required: true}
});

schema.plugin(autoIncrement.plugin, {model: 'Order', startAt: 10000});

schema.plugin(searchable);

module.exports = mongoose.model('Order', schema);