var express = require('express');
var router = express.Router();
var passport = require('passport');
var IndexController = require('../controllers/indexController');
var ProductController = require('../controllers/productController');
var LoginController = require('../controllers/logincontroller');
var CartController = require('../controllers/cartController');
var UserController = require('../controllers/userController');

/* GET home page. */
router.get('/', function (req, res, next) {
    IndexController.viewIndex(req, res, next);
});

router.get('/forgetpass', function (req, res, next) {
    res.render('contents/forgetpass', {title: 'Quên mật khẩu'});
});

router.get('/productsview/:idMaker', function (req, res, next) {
    ProductController.ProductView(req, res, next);
});

router.post('/search-product', function (req, res, next) {
    ProductController.ProductSearch(req, res, next);
});

router.get('/product-summary', isLogin, function (req, res, next) {
    CartController.Summary(req, res, next);
});

router.get('/compareproducts', function (req, res, next) {
    res.render('contents/compareproducts', {title: 'So sánh sản phẩm'});
});

router.get('/product-detail', function (req, res, next) {
    ProductController.ProductDetail(req, res, next);
});

router.get('/register', function (req, res, next) {
    if(req.isAuthenticated()) {
        return res.redirect('/');
    }
    res.render('contents/register', {title: 'Đăng ký tài khoản'});
});

router.post('/register', function (req, res, next) {
    LoginController.register(req, res, next);
});

router.get('/login', function (req, res, next) {
    if(req.isAuthenticated()) {
        return res.redirect('/');
    }
    res.render('contents/login', {title: 'Đăng nhập', errorLogin: req.flash('errorLogin')});
});

router.post('/login', passport.authenticate('local', {
    failureRedirect: '/login',
    failureFlash: { type: 'errorLogin', message: 'Thông tin tài khoản không hợp lệ' }}), function(req, res, next) {

    req.session.user = req.user;
    if(req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    }
    else {
        res.redirect('/');
    }
});

router.get('/verify', function (req, res, next) {
    LoginController.verify(req, res, next);
})

router.get('/logout', function(req, res, next) {
    req.logout();
    req.session.user = null;
    res.redirect('/');
});

router.get('/contact', function (req, res, next) {
    res.render('contents/contact', {title: 'Thông tin liên hệ', isContact: true});
});

router.get('/special_offers', function (req, res, next) {
    res.render('contents/special_offers', {title: 'Khuyến mãi đặc biệt'});
});

router.get('/add-to-cart/:id', function (req, res, next) {
    CartController.AddToCart(req, res, next);
});

router.get('/reduce/:id', isLogin, function (req, res, next) {
    CartController.Reduce(req, res, next);
});

router.get('/increase/:id', isLogin, function (req, res, next) {
    CartController.Increase(req, res, next);
});

router.get('/remove/:id', isLogin, function (req, res, next) {
    CartController.Remove(req, res, next);
});

router.post('/order', isLogin, function (req, res, next) {
    CartController.Order(req, res, next);
});

router.get('/profile', isLogin, function (req, res, next) {
    UserController.ViewProfile(req, res, next);
});

router.post('/update-profile', isLogin, function (req, res, next) {
    UserController.UpdateProfile(req, res, next);
});

router.get('/change-password', isLogin, function (req, res, next) {
    res.render('contents/change-password');
});

router.post('/change-password', isLogin, function (req, res, next) {
    UserController.ChangePassword(req, res, next);
});

router.get('/order-history', isLogin, function (req, res, next) {
    CartController.HistoryOrder(req, res, next);
});

router.get('/order-detail/:id', isLogin, function (req, res, next) {
    CartController.OrderDetail(req, res, next);
});

module.exports = router;

function isLogin(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/login');
}