var mongoose = require('mongoose');
var Product = require('../models/product');
var Specification = require('../models/specification');
var Maker = require('../models/maker');

mongoose.connect('localhost:27017/mobileshop');

for(i = 0; i < 40; i++) {

    var specIphone7 = new Specification({
        operatingSystem: 'iOS 10',
        monitor: 'LED-backlit IPS LCD, 5.5", Retina HD',
        cpu: 'Apple A10 Fusion 4 nhân 64-bit',
        ram: 3,
        internalMemory: 256,
        backCamera: 'Hai camera 12 MP',
        frontCamera: '7 MP',
        pin: '2900 mAh',
        connect: 'NFC, Air Play, OTG, HDMI',
        size: 'Dài 158.2 mm - Ngang 77.9 mm - Dày 7.3 mm'
    });
    specIphone7.save();

    var iphone7 = new Product({
        _id: 'iPhone7-Plus-256_' + i,
        productName: 'iPhone 7 Plus 256GB',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/iphone-7-plus-256gb.png',
        imgExtra1: '/images/iphone-7-plus-256gb.png',
        imgExtra2: '/images/iphone-7-plus-256gb.png',
        imgExtra3: '/images/iphone-7-plus-256gb.png',
        specification: specIphone7,
        maker: 'iphone',
        countSell: 0,
        dateAdd: 10/10/2011,
    });
    iphone7.save();
}

for(i = 0; i < 40; i++) {

    var specNexus = new Specification({
        operatingSystem: 'Android 4.4 (KitKat)',
        monitor: 'IPS LCD, 4.95", Full HD',
        cpu: 'Qualcomm MSM8974',
        ram: 2,
        internalMemory: 16,
        backCamera: '8 MP',
        frontCamera: '1.3 MP',
        pin: '2300 mAh',
        connect: '',
        size: 'Dài 137.9 mm - Ngang 69.2 mm - Dày 8.6 mm'
    });
    specNexus.save();

    var nexus = new Product({
        _id: 'LG-Nexus-5_' + i,
        productName: 'LG Nexus 5',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/nexus-5.jpg',
        imgExtra1: '/images/nexus-5.jpg',
        imgExtra2: '/images/nexus-5.jpg',
        imgExtra3: '/images/nexus-5.jpg',
        specification: specNexus,
        maker: 'asus',
        countSell: 0,
        dateAdd: 10/10/2011
    });
    nexus.save();
}

for(i = 0; i < 40; i++) {

    var specHuawei = new Specification({
        operatingSystem: 'Android 5.1 (Lollipop)',
        monitor: 'IPS LCD, 5", HD',
        cpu: 'MTK 6582 4 nhân 32-bit',
        ram: 1,
        internalMemory: 8,
        backCamera: '8 MP',
        frontCamera: '2 MP',
        pin: '2200 mAh',
        connect: '',
        size: 'Dài 143.8 mm - Ngang 72 mm - Dày 8.9 mm'
    });
    specHuawei.save();

    var huawei = new Product({
        _id: 'Huawei-Y5-II_' + i,
        productName: 'Huawei Y5 II',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/huawei-y5-ii.png',
        imgExtra1: '/images/huawei-y5-ii.png',
        imgExtra2: '/images/huawei-y5-ii.png',
        imgExtra3: '/images/huawei-y5-ii.png',
        specification: specHuawei,
        maker: 'huawei',
        countSell: 0,
        dateAdd: 10/10/2011
    });
    huawei.save();
}

// top sell
for(i = 41; i < 46; i++) {

    var specIphone7 = new Specification({
        operatingSystem: 'iOS 10',
        monitor: 'LED-backlit IPS LCD, 5.5", Retina HD',
        cpu: 'Apple A10 Fusion 4 nhân 64-bit',
        ram: 3,
        internalMemory: 256,
        backCamera: 'Hai camera 12 MP',
        frontCamera: '7 MP',
        pin: '2900 mAh',
        connect: 'NFC, Air Play, OTG, HDMI',
        size: 'Dài 158.2 mm - Ngang 77.9 mm - Dày 7.3 mm'
    });
    specIphone7.save();

    var iphone7 = new Product({
        _id: 'iPhone7-Plus-256_' + i,
        productName: 'iPhone 7 Plus 256GB',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/iphone-7-plus-256gb.png',
        imgExtra1: '/images/iphone-7-plus-256gb.png',
        imgExtra2: '/images/iphone-7-plus-256gb.png',
        imgExtra3: '/images/iphone-7-plus-256gb.png',
        specification: specIphone7,
        maker: 'iphone',
        countSell: 100,
        dateAdd: 10/10/2011,
    });
    iphone7.save();
}
for(i = 41; i < 46; i++) {

    var specNexus = new Specification({
        operatingSystem: 'Android 4.4 (KitKat)',
        monitor: 'IPS LCD, 4.95", Full HD',
        cpu: 'Qualcomm MSM8974',
        ram: 2,
        internalMemory: 16,
        backCamera: '8 MP',
        frontCamera: '1.3 MP',
        pin: '2300 mAh',
        connect: '',
        size: 'Dài 137.9 mm - Ngang 69.2 mm - Dày 8.6 mm'
    });
    specNexus.save();

    var nexus = new Product({
        _id: 'LG-Nexus-5_' + i,
        productName: 'LG Nexus 5',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/nexus-5.jpg',
        imgExtra1: '/images/nexus-5.jpg',
        imgExtra2: '/images/nexus-5.jpg',
        imgExtra3: '/images/nexus-5.jpg',
        specification: specNexus,
        maker: 'asus',
        countSell: 100,
        dateAdd: 10/10/2011
    });
    nexus.save();
}
for(i = 41; i < 46; i++) {

    var specHuawei = new Specification({
        operatingSystem: 'Android 5.1 (Lollipop)',
        monitor: 'IPS LCD, 5", HD',
        cpu: 'MTK 6582 4 nhân 32-bit',
        ram: 1,
        internalMemory: 8,
        backCamera: '8 MP',
        frontCamera: '2 MP',
        pin: '2200 mAh',
        connect: '',
        size: 'Dài 143.8 mm - Ngang 72 mm - Dày 8.9 mm'
    });
    specHuawei.save();

    var huawei = new Product({
        _id: 'Huawei-Y5-II_' + i,
        productName: 'Huawei Y5 II',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/huawei-y5-ii.png',
        imgExtra1: '/images/huawei-y5-ii.png',
        imgExtra2: '/images/huawei-y5-ii.png',
        imgExtra3: '/images/huawei-y5-ii.png',
        specification: specHuawei,
        maker: 'huawei',
        countSell: 100,
        dateAdd: 10/10/2011
    });
    huawei.save();
}

// new product
for(i = 47; i < 51; i++) {

    var specIphone7 = new Specification({
        operatingSystem: 'iOS 10',
        monitor: 'LED-backlit IPS LCD, 5.5", Retina HD',
        cpu: 'Apple A10 Fusion 4 nhân 64-bit',
        ram: 3,
        internalMemory: 256,
        backCamera: 'Hai camera 12 MP',
        frontCamera: '7 MP',
        pin: '2900 mAh',
        connect: 'NFC, Air Play, OTG, HDMI',
        size: 'Dài 158.2 mm - Ngang 77.9 mm - Dày 7.3 mm'
    });
    specIphone7.save();

    var iphone7 = new Product({
        _id: 'iPhone7-Plus-256_' + i,
        productName: 'iPhone 7 Plus 256GB',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/iphone-7-plus-256gb.png',
        imgExtra1: '/images/iphone-7-plus-256gb.png',
        imgExtra2: '/images/iphone-7-plus-256gb.png',
        imgExtra3: '/images/iphone-7-plus-256gb.png',
        specification: specIphone7,
        maker: 'iphone',
        countSell: 0,
        dateAdd: new Date(),
    });
    iphone7.save();
}
for(i = 47; i < 51; i++) {

    var specNexus = new Specification({
        operatingSystem: 'Android 4.4 (KitKat)',
        monitor: 'IPS LCD, 4.95", Full HD',
        cpu: 'Qualcomm MSM8974',
        ram: 2,
        internalMemory: 16,
        backCamera: '8 MP',
        frontCamera: '1.3 MP',
        pin: '2300 mAh',
        connect: '',
        size: 'Dài 137.9 mm - Ngang 69.2 mm - Dày 8.6 mm'
    });
    specNexus.save();

    var nexus = new Product({
        _id: 'LG-Nexus-5_' + i,
        productName: 'LG Nexus 5',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/nexus-5.jpg',
        imgExtra1: '/images/nexus-5.jpg',
        imgExtra2: '/images/nexus-5.jpg',
        imgExtra3: '/images/nexus-5.jpg',
        specification: specNexus,
        maker: 'asus',
        countSell: 0,
        dateAdd: new Date()
    });
    nexus.save();
}
for(i = 47; i < 51; i++) {

    var specHuawei = new Specification({
        operatingSystem: 'Android 5.1 (Lollipop)',
        monitor: 'IPS LCD, 5", HD',
        cpu: 'MTK 6582 4 nhân 32-bit',
        ram: 1,
        internalMemory: 8,
        backCamera: '8 MP',
        frontCamera: '2 MP',
        pin: '2200 mAh',
        connect: '',
        size: 'Dài 143.8 mm - Ngang 72 mm - Dày 8.9 mm'
    });
    specHuawei.save();

    var huawei = new Product({
        _id: 'Huawei-Y5-II_' + i,
        productName: 'Huawei Y5 II',
        price: 25990000,
        quantity: 29,
        imgMain: '/images/huawei-y5-ii.png',
        imgExtra1: '/images/huawei-y5-ii.png',
        imgExtra2: '/images/huawei-y5-ii.png',
        imgExtra3: '/images/huawei-y5-ii.png',
        specification: specHuawei,
        maker: 'huawei',
        countSell: 0,
        dateAdd: new Date()
    });
    huawei.save();
}

mongoose.disconnect();