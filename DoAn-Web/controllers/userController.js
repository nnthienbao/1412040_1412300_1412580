var User = require('../models/user');

exports.ViewProfile = function (req, res, next) {
   res.render('contents/profile', {title: 'Thông tin cá nhân'});
};

exports.UpdateProfile = function (req, res, next) {

    var fullname = req.body.fullname;
    var gender = req.body.gender=='male' ? true: false;
    var address = req.body.address;
    var phone = req.body.phone;

    req.user.fullname = fullname;
    req.user.gender = gender;
    req.user.address = address;
    req.user.phone = phone;

    req.user.save(function (err) {
       if(err){
           console.log(err);
       }
       res.render('contents/profile', {title: 'Thông tin cá nhân', message: 'Cập nhật thông tin thành công'});
    });
};

exports.ChangePassword = function (req, res, next) {

    req.checkBody('newpassword', 'Mật khẩu phải có ít nhất 4 ký tự').notEmpty().isLength({min: 4});
    req.checkBody('renewpassword', 'Mật khẩu xác thực không khớp').notEmpty().equals(req.body.newpassword);

    var errors = req.validationErrors();

    if(errors) {
        var messages = [];
        errors.forEach(function(err){
           messages.push(err.msg);
        });
        return res.render('contents/change-password', {title: 'Đổi mật khẩu', messages: messages, hasError: messages.length > 0});
    }

    var newPass = req.body.newpassword;

    req.user.setPassword(newPass, function () {
        req.user.save(function (err) {
           if(err) {
               console.log(err.message);
           }
       })
    });
    res.render('contents/change-password', {title: 'Đổi mật khẩu', success: true});
};