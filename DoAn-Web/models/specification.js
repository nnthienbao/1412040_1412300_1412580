var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    operatingSystem: {type: String},
    monitor: {type: String},
    cpu: {type: String},
    ram: {type: Number},
    internalMemory: {type: Number},
    backCamera: {type: String},
    frontCamera: {type: String},
    pin: {type: String},
    connect: {type: String},
    size: {type: String}
});

module.exports = mongoose.model('Specification', schema);