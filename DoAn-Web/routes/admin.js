var express = require('express');
var router = express.Router();
var passport = require('passport');
var multer  = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images/')
    },
    filename: function (req, file, cb) {
        var fileName = req.body.tensp.split(' ').join('-');
        cb(null, fileName + '-' + Date.now() + '.' + file.originalname.split('.').pop());
    }
});
var upload = multer({ storage: storage })

var ProductController = require('../controllers/admin/productController');
var OrderAdminController = require('../controllers/admin/orderAdminController');
var CustomerController = require('../controllers/admin/customerController');

router.get('/', function(req, res, next) {
    return res.redirect('/admin/product-view');
});

router.get('/logout', function(req, res, next) {
    req.logout();
    req.session.user = null;
    res.redirect('/admin/login');
});

router.get('/login', function(req, res, next) {
    if(req.isAuthenticated()) {
        return res.redirect('/admin');
    }
    var message = req.flash('errorLogin');
    res.render('contents/admin/login', {title: 'admin', layout: 'ad_layout', Login: true, message: message});
});

router.post('/login', passport.authenticate('local', {
    failureRedirect: '/admin/login',
    failureFlash: { type: 'errorLogin', message: 'Thông tin tài khoản không hợp lệ' }}), function(req, res, next) {

    req.session.user = req.user;
    res.redirect('/admin');
});

//  Sản phẩm
router.get('/product-view', isAdmin, function(req, res, next) {
    ProductController.productView(req, res, next);
});

router.get('/add-new-product', isAdmin, function(req, res, next) {
    ProductController.productInsertView(req, res, next);
});

router.get('/product-detail/:id', isAdmin, function(req, res, next) {
    ProductController.productDetail(req, res, next);
});

router.get('/edit-product/:id', isAdmin, function(req, res, next) {
    ProductController.productEdit(req, res, next);
});

var cpUpload = upload.fields([
    { name: 'imgmain', maxCount: 1 },
    { name: 'imgExtra1', maxCount: 1 },
    { name: 'imgExtra2', maxCount: 1 },
    { name: 'imgExtra3', maxCount: 1 }]);
router.post('/add-new-product', isAdmin, cpUpload, function (req, res, next) {
    ProductController.productInsert(req, res, next);
});

// router.post('/edit-product')

//  Kết thúc phần sản phẩm

//  Khách hàng
router.get('/customer-view',  isAdmin, function(req, res, next) {
    CustomerController.customerView(req, res, next);
});

router.get('/customer-detail/:id',  isAdmin, function (req, res, next) {
    CustomerController.customerDetail(req, res, next);
});

router.post('/customer-update-status',  isAdmin, function(req, res, next) {
    CustomerController.customerChangeStatus(req, res, next);
});

router.post('/customer-search', isAdmin, function (req, res, next) {
    CustomerController.customerFind(req, res, next);
});
//  Kết thúc phần khách hàng

//  Hóa đơn
router.get('/view-order', isAdmin, function(req, res, next) {
    OrderAdminController.ViewOrders(req, res, next);
});

router.get('/view-order-detail/:id', isAdmin, function(req, res, next) {
    OrderAdminController.ViewOrderDetail(req, res, next);
});

router.post('/update-status-order', isAdmin, function(req, res, next) {
    OrderAdminController.ChangeStatus(req, res, next);
});

router.post('/search-order', isAdmin, function(req, res, next) {
    OrderAdminController.SearchOrder(req, res, next);
});
//  Kết thúc phần hóa đơn

router.post('/delete-product', isAdmin, function(req, res, next) {
    ProductController.productDelete(req, res, next);
});

module.exports = router;

function isAdmin(req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.hasAccess('admin')) {
            return next();
        }
        return res.send(403);
    }
    res.redirect('/admin/login');
}