var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongooseEmail = require('passport-local-mongoose-email');
var autoIncrement = require('mongoose-auto-increment');

var connection = mongoose.connection;

autoIncrement.initialize(connection);

var schema = new Schema({
    email: {type: String, required: true},
    fullname: {type: String, required: true},
    gender: {type: Boolean, required: true},
    address: {type: String, required: true},
    phone: {type: String, required: true}
});

schema.plugin(passportLocalMongooseEmail, {
    usernameField: 'email',
    userExistsError: 'Đã tồn tại tài khoản với email %s'
});

schema.plugin(autoIncrement.plugin, {model: 'User', startAt: 10000});

schema.plugin(require('mongoose-role'), {
    roles: ['public', 'user', 'admin'],
    accessLevels: {
        'public': ['public', 'user', 'admin'],
        'anon': ['public'],
        'user': ['user', 'admin'],
        'admin': ['admin']
    }
});

module.exports = mongoose.model('User', schema);