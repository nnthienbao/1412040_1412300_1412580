var Product = require('../models/product');
var Maker = require('../models/maker');
var Cart = require('../models/cart');
var Order = require('../models/order');

exports.AddToCart = function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, function (err, product) {
        if(err) {
            return res.redirect('/');
        }
        cart.add(product, productId);
        req.session.cart = cart;
        console.log(req.session.cart);
        res.redirect('/');
    });
};

exports.Summary = function (req, res, next) {
    if(!req.session.cart) {
        return res.render('contents/product-summary', {title: 'Giỏ hàng', items: null});
    }
    var cart = new Cart(req.session.cart);
    res.render('contents/product-summary', {title: 'Giỏ hàng', items: cart.generateArray(), totalPrice: cart.totalPrice});
};

exports.Reduce = function (req, res, next) {
    var idProduct = req.params.id;
    var cart = new Cart(req.session.cart);
    cart.reduce(idProduct);

    req.session.cart = cart.totalQty > 0 ? cart : null ;

    res.redirect('/product-summary');
};

exports.Increase = function (req, res, next) {
    var idProduct = req.params.id;
    var cart = new Cart(req.session.cart);
    cart.increase(idProduct);

    req.session.cart = cart;

    res.redirect('/product-summary');
};

exports.Remove = function (req, res, next) {
    var idProduct = req.params.id;
    var cart = new Cart(req.session.cart);
    cart.remove(idProduct);

    req.session.cart = cart.totalQty > 0 ? cart : null ;

    res.redirect('/product-summary');
};

exports.Order = function (req, res, next) {
    if(!req.session.cart) {
        return redirect('/');
    }
    var address = req.body.address;
    var phone = req.body.phone;
    var user = req.user;
    var cart = new Cart(req.session.cart);

    var order = new Order({
        user: user,
        cart: cart,
        address: address,
        phone: phone,
        date: new Date(),
        status: 'PROCESSING'
    });
    order.save(function (err) {
        if(err) {
            return res.render('contents/order-info', {title: 'Đặt hàng không thành công', success: false});
        }
        req.session.cart = null;
        return res.render('contents/order-info', {title: 'Đặt hàng thành công', success: true});
    });
};

exports.HistoryOrder = function (req, res, next) {
    Order.find({user: req.user}, function (err, orders) {
        if(err) {
            console.log(err.message);
        }
        res.render('contents/order-history', {title: 'Lịch sử giao dịch', orders: orders});
    });
};

exports.OrderDetail = function (req, res, next) {
    var id = req.params.id;
    Order.findById(id).populate('user').exec(function (err, order) {
        if(err) {
            console.log(err.message);
        }
        if(!order || order.user._id != req.user._id) {
            return res.redirect('/');
        }
        res.render('contents/order-detail', {title: 'Mã đơn hàng ' + id, order: order});
    });
};













