var fromEmail = 'mobileshop@doanweb.com';
var subject = 'Confirm your email';
var sg = require('sendgrid')('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');

exports.send = function (req, res, next, user) {
    var toEmail = user.email;
    var authenticationURL = req.protocol + '://' + req.get('host') + '/verify?authToken=' + user.authToken;
    console.log(authenticationURL);
    var content = '<p>Nhấn vào liên kết bên dưới để kích hoạt tài khoản</p><a target=_blank href="' + authenticationURL + '">Confirm your email</a>';

    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: {
            personalizations: [
                {
                    to: [
                        {
                            email: toEmail
                        }
                    ],
                    subject: subject
                }
            ],
            from: {
                email: fromEmail
            },
            content: [
                {
                    type: 'text/html',
                    value: content
                }
            ]
        }
    });
    sg.API(request, function (error, response) {
        if (error) {
            console.log('Error response received');
        }
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
        res.render('contents/email-verification', {title: 'Xác thực email'});
    });
}