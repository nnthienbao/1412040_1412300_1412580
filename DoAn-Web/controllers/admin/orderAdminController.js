var Order = require('../../models/order');

exports.ViewOrders = function (req, res, next) {

    var sort = req.query.sort;
    var by = req.query.by;
    var optionsSort = {};
    if(sort == 'price') {
        optionsSort = {'cart.totalPrice': by};
    }
    if(sort == 'date') {
        optionsSort.date = by;
    }

    Order.find({}).populate('user').sort(optionsSort).exec(function (err, orders) {
       if(err) {
           console.log(err.message);
       }
        res.render('contents/admin/view-order', {title: 'Quản lý đơn hàng',  layout: 'ad_layout', orders: orders});
    });
};

exports.ViewOrderDetail = function (req, res, next) {
    var id = req.params.id;
    Order.findById(id).populate('user').exec(function (err, order) {
        if(err) {
            console.log(err.message);
        }
        res.render('contents/admin/view-order-detail', {title: 'Thông tin đơn đặt hàng ' + id,  layout: 'ad_layout', order: order});
    });
};

exports.SearchOrder = function (req, res, next) {
    var keyword = req.body.keyword;
    console.log(keyword);
    Order.find({_id: keyword}).populate('user').exec(function (err, orders) {
       if(err){
           console.log(err.message);
       }
       res.render('contents/admin/view-order', {title: 'Quản lý đơn hàng',  layout: 'ad_layout', orders: orders});
    });
};

exports.ChangeStatus = function (req, res, next) {
    var id = req.body.idOrder;
    var status = req.body.status;
    Order.findById(id).exec(function (err, order) {
        if(err) {
            console.log(err.message);
        }
        order.status = status;
        order.save(function (err) {
            if(err) {
                console.log(err.message);
            }
            res.redirect('/admin/view-order-detail/' + id);
        });

    });
};
