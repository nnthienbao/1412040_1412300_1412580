var Maker = require('../models/maker');
var Product = require('../models/product')

exports.viewIndex = function (req, res, next) {
    var limitItem = 16;

    Product.find({}).sort({countSell: -1}).limit(limitItem).exec(function (err, topSells) {
        var topSellChunks = [];
        var chunkTopSellSize = 4;
        for (var i = 0; i < topSells.length; i += chunkTopSellSize) {
            topSellChunks.push(topSells.slice(i, i + chunkTopSellSize));
        }

        Product.find({}).sort({dateAdd: -1}).limit(limitItem).exec(function (err, newProducts) {
            var newProductChunks = [];
            var chunkNewProductSize = 4;
            for (var i = 0; i < newProducts.length; i += chunkNewProductSize) {
                newProductChunks.push(newProducts.slice(i, i + chunkNewProductSize));
            }

            res.render('contents/index', {
                title: 'Trang chủ',
                topSellChunks: topSellChunks, newProducts: newProductChunks, isIndex: true
            });
        });
    });
}